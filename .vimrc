
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                         default .vimrc                                "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" source ~/.vimrc_default

set noundofile
set cursorline
set shellcmdflag=-ic

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                          basic setting                                "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" let mapleader = ","
" let maplocalleader = ","

" set nu
set backupdir=~/.vim/backup//
" set nofixendofline
set numberwidth=6
set tabstop=4
set softtabstop=-1
set shiftwidth=4
set hls
" 修改 comment 的顏色
highlight Comment ctermfg=darkgray

" 補齊指定的 shift 寬度, 有對齊的效果
set shiftround
" 把 tab 換成 space
set expandtab
" 提示目前輸入命令的狀態列
set showcmd
" 距離視窗第幾行開始捲動
set scrolloff=3

" show tab bar always
set showtabline=2

set nobackup
set noswapfile

" :help folding, za/zr/zm/zR/zM/zj/zk
"
let $is_folded=1
function SwitchFold()
    if $is_folded==0
        :exe "normal zR"
        let $is_folded=1
    else
        :exe "normal zM"
        let $is_folded=0
    endif
endfunction

set foldmethod=indent
nnoremap <space> za
vnoremap <space> zf
au BufRead * normal zR
au BufWinEnter * normal zR
nnoremap <F6> :call SwitchFold()<CR>


" let g:netrw_banner = 1
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3
" " let g:netrw_altv = 1
" netrw, http://vimdoc.sourceforge.net/htmldoc/pi_netrw.html#g:netrw_list_hide
" let specific extension file hide
let g:netrw_winsize = 30
let g:netrw_list_hide= '.*\.swp$,.*\.pyc$,__pycache__,\.pytest_cache/,\.git/,_venv/,.*\.egg-info/,build/,.eggs/'

" FIX: <C+l> at netrw
" https://stackoverflow.com/questions/33351179/how-to-rewrite-existing-vim-key-bindings
" :verbose nmap <c-l>
nmap <unique> <c-a> <Plug>NetrwRefresh


" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Texplore
" augroup END

" Commenting blocks of code.
autocmd FileType c,cpp,java,scala,javascript let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> ,cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> ,cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

" statusline show full file path
" or {count}ctrl+G , count==1, to show full path
set statusline=%F
set statusline+=\ -\        " Separator
set statusline+=FileType:   " Label
set statusline+=%y          " Filetype of the file
set statusline+=\ -\        " Separator
set statusline+=%l          " Current line
set statusline+=/           " Separator
set statusline+=%L          " Total lines


nnoremap <leader>sv :so $MYVIMRC<cr>
nnoremap <leader>ev :tabe $MYVIMRC<cr>
nnoremap <leader>emv :tabe ~/vim_cfg/.vimrc<cr>
nnoremap <leader>f :Vexplore<cr>

nnoremap <leader>eall :tabe ~/.vim/UltiSnips/all.snippets<cr>

nnoremap <leader>ejs :Sexplore ~/.vim/UltiSnips/javascript<cr>
nnoremap <leader>epy :Sexplore ~/.vim/UltiSnips/python<cr>
nnoremap <leader>emd :Sexplore ~/.vim/UltiSnips/markdown<cr>

nnoremap <leader>rjs :tab sview ~/.vim/plugged/vim-snippets/UltiSnips/javascript.snippets<cr>
nnoremap <leader>rpy :tab sview ~/.vim/plugged/vim-snippets/UltiSnips/python.snippets<cr>

nnoremap <leader>suw :w !sudo tee %
nnoremap <leader>nu :set nu! <cr>
nnoremap <leader>rnu :set rnu! <cr>

nnoremap <C-h> gT
nnoremap <C-l> gt

" 選擇文字然後用 quote 符號包起來
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel<Paste>
nnoremap <leader>' viw<esc>a'<esc>hbi'<esc>lel
nnoremap <S-h> ^
nnoremap <S-l> $

"" 改由 jk 或 kj 來離開 i-mode
inoremap jk<space> <esc>
inoremap kj<space> <esc>
" inoremap <esc> <nop>

augroup for_js
    autocmd!
    " 設置附檔名 .ejs 為 javascript 
    autocmd BufNewFile,BufRead *.ejs set filetype=javascript
    " 快速加註解 for javascript
    autocmd FileType javascript nnoremap <buffer> <LocalLeader>c I//<esc>
    "autocmd FileType javascript :iabbrev <buffer> iff if ()<left>
augroup end


nnoremap <F2> :<C-U>setlocal lcs=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:- list! list? <CR>
nnoremap <F3> :GitGutterBufferToggle<CR>
nnoremap <F4> :set nu!<CR>
nnoremap <F5> :set invpaste paste?<CR>
" https://stackoverflow.com/questions/18948491/running-python-code-in-vim
nnoremap <F8> :! clear; pycodestyle %<CR>
nnoremap <F9> :! clear; python %<CR>
nnoremap <F10> :! clear; python -m unittest<CR>

set pastetoggle=<F5>
set showmode

" nnoremap <F4> :!ctags -R<CR>

" 設定搜尋的高亮顏色
" highlight Search ctermbg=1

" for statusline
set laststatus=2
" set statusline=%4*%<\ %1*[%F]
" set statusline+=%4*\ %5*[%{&encoding}, " encoding
" set statusline+=%{&fileformat}%{\"\".((exists(\"+bomb\")\ &&\ &bomb)?\",BOM\":\"\").\"\"}]%m
" set statusline+=%4*%=\ %6*%y%4*\ %3*%l%4*,\ %3*%c%4*\ \<\ %2*%P%4*\ \>
" highlight User1 ctermfg=red
" highlight User2 term=underline cterm=underline ctermfg=green
" highlight User3 term=underline cterm=underline ctermfg=yellow
" highlight User4 term=underline cterm=underline ctermfg=white
" highlight User5 ctermfg=cyan
" highlight User6 ctermfg=white


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       vim-plug setting                                "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin('~/.vim/plugged')

" Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }


" "https://github.com/neoclide/coc.nvim
Plug 'neoclide/coc.nvim', {'branch': 'release'}



" "NERDTree
" Plug 'scrooloose/nerdtree'
" :nnoremap <F5> :NERDTreeToggle <CR>
" 
" 
" "NERDTreeTabs
" " https://github.com/jistr/vim-nerdtree-tabs
" Plug 'jistr/vim-nerdtree-tabs'
" :nnoremap <F4> :NERDTreeTabsToggle <CR>
" :let g:nerdtree_tabs_open_on_console_startup = 1


" SuperTab
Plug 'ervandew/supertab'
"let g:SuperTabRetainCompletionType=2
let g:SuperTabDefaultCompletionType="<C-X><C-O>"

" Group dependencies, vim-snippets depends on ultisnips
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
let g:UltiSnipsExpandTrigger="<c-j>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsListSnippets="<c-k>"

" https://stackoverflow.com/questions/32156837/how-to-create-my-custom-vim-snippets
" let g:UltiSnipsSnippetsDir='~/.vim/my-snippets/'


" omnifuncs
augroup omnifuncs
  autocmd!
  autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
  autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
  autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
  autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
  autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
augroup end


" [Q&A]
" Q. Undefined function pythoncomplete#Complete
" A. https://github.com/Shougo/neocomplcache.vim/issues/509


" Plug 'Shougo/unite.vim'
" nnoremap <C-p> :Unite -start-insert file_rec -smartcase<CR>
" nnoremap <leader>b :Unite buffer<CR>
" 
" augroup for_unite
"     autocmd!
"     autocmd FileType unite call s:unite_settings()
" augroup end
" 
" function! s:unite_settings()
"   inoremap <buffer> <C-j> <Plug>(unite_select_next_line)
"   inoremap <buffer> <C-k> <Plug>(unite_select_previous_line)
" 
"   "nmap <silent><buffer><expr> Enter unite#do_action('switch')
"   nnoremap <silent><buffer><expr> <CR> unite#do_action('tabswitch')
"   nnoremap <silent><buffer><expr> <C-h> unite#do_action('splitswitch')
"   nnoremap <silent><buffer><expr> <C-v> unite#do_action('vsplitswitch')
" 
"   "imap <silent><buffer><expr> Enter unite#do_action('switch')
"   inoremap <silent><buffer><expr> <CR> unite#do_action('tabswitch')
"   inoremap <silent><buffer><expr> <C-h> unite#do_action('splitswitch')
"   inoremap <silent><buffer><expr> <C-v> unite#do_action('vsplitswitch')
" 
"   noremap <buffer> <C-p> <Plug>(unite_toggle_auto_preview)
" 
"   nnoremap <ESC> :UniteClose<cr>
" endfunction


" git diff marker
set updatetime=100
Plug 'airblade/vim-gitgutter'


Plug 'itchyny/lightline.vim'
" Replace filename component of Lightline statusline
let g:lightline = {
      \ 'component_function': {
      \   'filename': 'FilenameForLightline'
      \ }
      \ }
 
" Show full path of filename
function! FilenameForLightline()
    return expand('%')

" return expand('%:p')
" :echo @%                |" directory/name of file
" :echo expand('%:t')     |" name of file ('tail')
" :echo expand('%:p')     |" full path
" :echo expand('%:p:h')   |" directory containing file ('head')
endfunction



Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
noremap <buffer> <C-p> :Files<CR>


" Plug 'plasticboy/vim-markdown'


" Plug 'davidhalter/jedi-vim'
" " " :help jedi-vim
" " https://github.com/davidhalter/jedi-vim/blob/master/doc/jedi-vim.txt
" 
" " let g:jedi#auto_initialization = 0
" " let g:jedi#auto_vim_configuration = 0
" let g:jedi#use_tabs_not_buffers = 1
" let g:jedi#use_splits_not_buffers = "left"
" " let g:jedi#popup_on_dot = 0
" " let g:jedi#popup_select_first = 0
" " let g:jedi#show_call_signatures = "1"
" " let g:jedi#goto_command = "<leader>d"
" " let g:jedi#goto_assignments_command = "<leader>g"
" " let g:jedi#goto_definitions_command = ""
" " let g:jedi#documentation_command = "K"
" let g:jedi#usages_command = "<leader>m"
" " let g:jedi#completions_command = "<C-Space>"
" " let g:jedi#rename_command = "<leader>r"
" " let g:jedi#completions_enabled = 0



call plug#end()
" vim-plug end


