#!/bin/sh

## auto install vim plugins

function install_plug(){
    #https://github.com/junegunn/vim-plug
    curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
            https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
}

function make_vimrc_ln(){
    ln -s ~/vim_cfg/.vimrc ~/.vim/vimrc
    ln -s ~/vim_cfg/UltiSnips ~/.vim/UltiSnips
}

function plug_install(){
    #https://github.com/junegunn/vim-plug/issues/225
    vim +PlugInstall +qall
}

function install(){
    install_vim_plug
    make_vimrc_ln
    plug_install
}

install


