# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* My Personal VIM Configuration

### How do I get set up? ###

* cd ~
* git clone git@bitbucket.org:ueicren/cfg.git vim_cfg
* I use vim-plug to install plugins, so install https://github.com/junegunn/vim-plug
* vim ~/.vimrc
> * plus one line at bottom: source ~/vim_cfg/.vimrc
> * :PlugInstall
> * :w
> * :q or :source ~/.vimrc
* done!